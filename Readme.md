# pcap-util

Functions to operate pcap file.

## Usage

Import-Module the psm1 file. If you are at this project's root:

```PowerShell
Import-Module -Name ".\src\PcapUtil\PcapUtil.psm1"
```

Then functions will be available:

```PowerShell
Import-Pcap -Path "some-pcap-file.pcap"
```

Import-Pcap uses tshark.exe. It expects tshark.exe to be in one of these paths:

```PowerShell
        $TsharkPathList = @(
            "C:\Program Files\Wireshark\tshark.exe"
            "C:\Program Files (x86)\Wireshark\tshark.exe"
        )
```

## Thanks to

- [Wireshark](https://wiki.wireshark.org/)
    - This project uses tshark.exe. It becomes available when Wireshark is installed. (This project does not contain tshark.exe or Wireshark installer.)
    - This project contains c1222_std_example8.pcap for testing. It came from [Wireshark's GitLab site](https://gitlab.com/wireshark/wireshark/-/blob/wireshark-4.0.1/test/captures/c1222_std_example8.pcap) .
