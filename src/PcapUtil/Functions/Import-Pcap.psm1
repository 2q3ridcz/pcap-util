﻿function Import-Pcap {
    [CmdletBinding()]
    [OutputType([object])]
    param (
        [Parameter(
            Mandatory=$true,
            ValueFromPipeline=$true
        )]
        [string]
        $Path
    )

    begin {
        $TsharkPathList = @(
            "C:\Program Files\Wireshark\tshark.exe"
            "C:\Program Files (x86)\Wireshark\tshark.exe"
        )

        $TsharkPath = $Null
        foreach ($TPath in $TsharkPathList) {
            If (Test-Path -Path $TPath -PathType Leaf) {
                $TsharkPath = $TPath
                Break
            }
        }
        If ($Null -Eq $TsharkPath) {
            throw "Could not find tshark.exe."
        }

        $Header = @(
            "frame.number"
            "frame.time_epoch"
            "ip.src"
            "ip.dst"
            "_ws.col.Protocol"
            "tcp.srcport"
            "tcp.dstport"
            "tcp.stream"
            "tcp.len"
            "tcp.seq"
            "tcp.ack"
            "tcp.seq_raw"
            "tcp.ack_raw"
            "tcp.flags"
            "_ws.col.Info"
            "tcp.payload"
            "data.data"
        )
    }

    process {
        $File = Get-Item -Path $Path
        If ($Null -Eq $File) {
            return
        }

        $TcpCsv = &($TsharkPath) -r $File.FullName `
            -T fields `
            -e frame.number `
            -e frame.time_epoch `
            -e ip.src `
            -e ip.dst `
            -e _ws.col.Protocol `
            -e tcp.srcport `
            -e tcp.dstport `
            -e tcp.stream `
            -e tcp.len `
            -e tcp.seq `
            -e tcp.ack `
            -e tcp.seq_raw `
            -e tcp.ack_raw `
            -e tcp.flags `
            -e _ws.col.Info `
            -e tcp.payload `
            -e data.data `
            -E separator="," -E quote=d

        $Tcp = $TcpCsv | ConvertFrom-Csv -Header $Header
        $Tcp |
        %{
            # Replace garbled words in Japanese environment.
            $_."_ws.col.Info" = $_."_ws.col.Info".Replace("竊・", "→ ")
        }

        $Tcp | Write-Output
    }

    end {}
}