﻿<#
.Synopsis
    Converts epochtime to datetime.
.DESCRIPTION
    Converts epochtime to datetime.

    If TimeZoneId parameter is not set, the cmdlet returns datetime in utc timezone.
    To see how to change the timezone, check the TimeZoneId parameter section.
.PARAMETER EpochTime
    Epochtime. Milliseconds are supported, but not micro or nanoseconds.
.PARAMETER TimeZoneId
    When not set or "" is set, the cmdlet returns datetime in utc timezone.

    When "Local" is set, the cmdlet returns datetime in local timezone.
    You can check your local timezone by ```[TimeZoneInfo]::Local```

    When a timezone id is set, the cmdlet returns datetime in that timezone.
    You can find timezone id by ```[TimeZoneInfo]::GetSystemTimeZones()```
.EXAMPLE
    ConvertFrom-EpochTime -EpochTime 1380138280.000

    Converts epochtime to datetime. Default timezone is utc.
.EXAMPLE
    ConvertFrom-EpochTime -EpochTime 1380138280.000 -TimeZoneId "Tokyo Standard Time"

    Converts epochtime to datetime in Tokyo timezone.
#>
function ConvertFrom-EpochTime {
    [CmdletBinding()]
    [OutputType([Datetime])]
    param (
        # パラメーター 1 のヘルプの説明
        [Parameter(
            Mandatory=$true,
            ValueFromPipeline=$true
        )]
        [double]
        $EpochTime
        ,
        [Parameter(Mandatory=$false)]
        [string]
        $TimeZoneId = ""
    )

    begin {}

    process {
        $Datetime = (Get-Date -Date "01-01-1970") + ([System.TimeSpan]::FromSeconds($EpochTime))
        If ($TimeZoneId -Ne "") {
            If ($TimeZoneId -Eq "Local") {
                $TimeZoneInfo = [TimeZoneInfo]::Local
            } Else {
                $TimeZoneInfo = [TimeZoneInfo]::FindSystemTimeZoneById($TimeZoneId)
            }
            $Datetime = [TimeZoneInfo]::ConvertTimeFromUtc($Datetime, $TimeZoneInfo)
        }
        $Datetime | Write-Output
    }

    end {}
}
