﻿$here = Split-Path -Parent $MyInvocation.MyCommand.Path
.("$here\..\Import-TargetPackage.ps1")
$here = Split-Path -Parent $MyInvocation.MyCommand.Path

Describe "ConvertFrom-EpochTime" {
    Context "EpochTime" {
        $testCases = @(
            @{ Case = "seconds (no round)"      ; EpochTime = 1380138280.000000000; Expect = (Get-Date -Date "2013-09-25 19:44:40") }
            @{ Case = "millieconds (no round)"  ; EpochTime = 1380138280.001000000; Expect = (Get-Date -Date "2013-09-25 19:44:40.001") }
            @{ Case = "microseconds (rounddown)"; EpochTime = 1380138280.000499000; Expect = (Get-Date -Date "2013-09-25 19:44:40") }
            @{ Case = "microseconds (roundup)"  ; EpochTime = 1380138280.000500000; Expect = (Get-Date -Date "2013-09-25 19:44:40.001") }
            @{ Case = "nanoseconds (rounddown) WEIRD!!"; EpochTime = 1380138280.000499849; Expect = (Get-Date -Date "2013-09-25 19:44:40") }
            @{ Case = "nanoseconds (roundup) WEIRD!!"  ; EpochTime = 1380138280.000499850; Expect = (Get-Date -Date "2013-09-25 19:44:40.001") }
        )
        It "Returns datetime to milliseconds (input order: <Case>)" -TestCases $testCases {
            # Given
            param ($Case, $EpochTime, $Expect)
            # When
            $Datetime = ConvertFrom-EpochTime -EpochTime $EpochTime
            # Then
            $Datetime | Should Be $Expect
        }

        $testCases = @(
            @{ Case = "Under min"; EpochTime = -62135596800.001; Expect = "Throws" }
            @{ Case = "Min"; EpochTime = -62135596800.000; Expect = (Get-Date -Date "0001-01-01 00:00:00") }
            @{ Case = "Zero"; EpochTime = 0.000; Expect = (Get-Date -Date "1970-01-01 00:00:00") }
            @{ Case = "Max"; EpochTime = 253402300799.999; Expect = (Get-Date -Date "9999-12-31 23:59:59.999") }
            @{ Case = "Over max"; EpochTime = 253402300800.000; Expect = "Throws" }
        )
        It "<Expect> given <EpochTime> (<Case>)" -TestCases $testCases {
            # Given
            param ($EpochTime, $Expect)
            # When # Then
            If ($Expect -Eq "Throws") {
                {ConvertFrom-EpochTime -EpochTime $EpochTime} | Should Throw
            } Else {
                ConvertFrom-EpochTime -EpochTime $EpochTime | Should Be $Expect
            }
        }
    }

    Context "TimeZoneId" {
        $testCases = @(
            @{ Case = "UTC"; TimeZoneId = $Null                ; Expect = (Get-Date -Date "2013-09-25 19:44:40") }
            @{ Case = "UTC"; TimeZoneId = ""                   ; Expect = (Get-Date -Date "2013-09-25 19:44:40") }
            @{ Case = "UTC"; TimeZoneId = "Utc"                ; Expect = (Get-Date -Date "2013-09-25 19:44:40") }
            @{ Case = "JST"; TimeZoneId = "Tokyo Standard Time"; Expect = (Get-Date -Date "2013-09-26 04:44:40") }
            @{ Case = "Local"; TimeZoneId = "Local"            ; Expect = "" }
        )
        It "Returns datetime in <Case> timezone given '<TimeZoneId>'" -TestCases $testCases {
            # Given
            param ($Case, $TimeZoneId, $Expect)
            $EpochTime = 1380138280.000000000
            # When
            $Datetime = ConvertFrom-EpochTime -EpochTime $EpochTime -TimeZoneId $TimeZoneId
            # Then
            If ($Case -Eq "Local") {
                $UtcTime = ConvertFrom-EpochTime -EpochTime $EpochTime -TimeZoneId "Utc"
                $LocalTime = ([TimeZone]::CurrentTimeZone).ToLocalTime($UtcTime)
                $Datetime | Should Be $LocalTime
            } Else {
                $Datetime | Should Be $Expect
            }
        }

        It "Throws given invalid value" {
            # Given
            $EpochTime = 1380138280.000000000
            # When # Then
            {ConvertFrom-EpochTime -EpochTime $EpochTime -TimeZoneId "TimeZoneId"} | Should Throw
        }
    }
}
