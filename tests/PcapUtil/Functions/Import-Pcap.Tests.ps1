﻿$here = Split-Path -Parent $MyInvocation.MyCommand.Path
.("$here\..\Import-TargetPackage.ps1")
$here = Split-Path -Parent $MyInvocation.MyCommand.Path

# Set $Skip = $False to test.
# Testcases here fails when tshark.exe is not found.
$Skip = $True

Describe "Import-Pcap" {
    Context "Path" {
        It "Returns tcp objects" -Skip:$Skip {
            # Given
            $File = Get-Item -Path "$here\..\TestFile\フォルダ\c1222_std_example8_日本語入りファイル名.pcap"
            # When
            $TcpList = Import-Pcap -Path $File.FullName
            # Then
            $TcpList.Length | Should Be 2
        }

        It "Accepts single pipline input" -Skip:$Skip {
            # Given
            $File = Get-Item -Path "$here\..\TestFile\フォルダ\c1222_std_example8_日本語入りファイル名.pcap"
            # When
            $TcpList = $File.FullName | Import-Pcap
            # Then
            $TcpList.Length | Should Be 2
        }

        It "Accepts multiple pipline input" -Skip:$Skip {
            # Given
            $File = Get-Item -Path "$here\..\TestFile\フォルダ\c1222_std_example8_日本語入りファイル名.pcap"
            $PathList = @(
                $File.FullName
                $File.FullName
            )
            # When
            $TcpList = $PathList | Import-Pcap
            # Then
            $TcpList.Length | Should Be (2 * 2)
        }
    }

    <# Mocking seems to be not working...
    Context "TsharkPath variable" {
        It "Throws when tshark is not found" {
            # Given
            Mock -CommandName Test-Path -MockWith { return $false } -ModuleName PcapUtil
            $File = Get-Item -Path "$here\..\TestFile\フォルダ\200722_win_scale_examples_anon_日本語入りファイル名.pcapng"
            # When # Then
            {Import-Pcap -Path $File.FullName} | Should Throw
            Assert-VerifiableMocks
            Assert-MockCalled -CommandName Test-Path -Times 2 -ModuleName PcapUtil
        }
    }
    #>
}
