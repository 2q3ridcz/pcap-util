﻿$ThisFile = Get-Item -Path $MyInvocation.MyCommand.Path
$here = $ThisFile.DirectoryName
Push-Location -Path $here

"# " + $ThisFile.Name | %{ (Get-Date).ToString("yyyy/MM/dd HH:mm:ss") + " " + $_ } | Write-Host
"here: " + $here | Write-Host

"## " + 'Start Logging...' | %{ (Get-Date).ToString("yyyy/MM/dd HH:mm:ss") + " " + $_ } | Write-Host
$LogFolder = Get-Item -Path "$here\Log"
$LogFileName = (Get-Date).ToString("yyyyMMdd-HHmmss") + "_" + $ThisFile.Name + "_Log.txt"
$LogFilePath = $LogFolder.FullName | Join-Path -ChildPath $LogFileName
Start-Transcript -Path $LogFilePath


"## " + 'Importing modules...' | %{ (Get-Date).ToString("yyyy/MM/dd HH:mm:ss") + " " + $_ } | Write-Host
$ProjectFolder = Get-Item -Path "$here\..\"
foreach ($PackageName in @("PcapUtil")) {
    $PackagePath = $ProjectFolder.FullName | Join-Path -ChildPath "src\$PackageName\$PackageName.psm1"
    Import-Module -Name $PackagePath -Force
}


"## " + 'Parse pcap file and output GridView...' | %{ (Get-Date).ToString("yyyy/MM/dd HH:mm:ss") + " " + $_ } | Write-Host
$Path = "$here\..\tests\PcapUtil\TestFile\フォルダ\c1222_std_example8_日本語入りファイル名.pcap"
Import-Pcap -Path $Path |
%{
    $Datetime = $_."frame.time_epoch" | ConvertFrom-EpochTime -TimeZoneId "Tokyo Standard Time"
    Add-Member -InputObject $_ -MemberType NoteProperty -Name "Datetime" -Value $Datetime -PassThru
} | Out-GridView

"## " + 'End Logging...' | %{ (Get-Date).ToString("yyyy/MM/dd HH:mm:ss") + " " + $_ } | Write-Host
Stop-Transcript
Pop-Location

Read-Host -Prompt ("Fin! Press enter to exit...")
